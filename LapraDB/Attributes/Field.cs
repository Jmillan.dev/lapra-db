﻿using System;

namespace LapraDB.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FieldAttribute : Attribute
    {
        public FieldAttribute(string name, string display = "")
        {
            Name = name;
            Display = display;
        }

        public string Name { get; } = null;
        public string Display { get; } = null;
        public string FunctionName { get; set; } = null;
        public bool readOnly { get; set; } = false;
        public bool writeOnly { get; set; } = false;
    }
}