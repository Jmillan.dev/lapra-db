﻿using System;

namespace LapraDB.Attributes.Validators
{
    public class MinAttribute : Validator
    {
        public MinAttribute(object value)
        {
            Value = (IComparable)value;
        }

        public override bool Validate(object value)
        {
            // compareTo can return  0 on equals
            // compareTo can return  1 on first value being bigger
            // compareTo can return -1 on second value being bigger

            return Value.CompareTo(value) != 1;
        }

        public IComparable Value { get; }
    }
}