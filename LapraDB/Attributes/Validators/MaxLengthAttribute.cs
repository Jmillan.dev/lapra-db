﻿using System;

namespace LapraDB.Attributes.Validators
{
    public class MaxLengthAttribute : Validator
    {
        public MaxLengthAttribute(int length)
        {
            Length = length;
        }

        public override bool Validate(object value)
        {
            // do we really need to check if the object it's a string?
            var str = (string)value; // try to cast it into a string

            return str.Length >= Length;
        }

        public int Length { get; }
    }
}