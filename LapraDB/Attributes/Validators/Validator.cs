﻿using System;

namespace LapraDB.Attributes.Validators
{
    [AttributeUsage(AttributeTargets.Property)]
    public abstract class Validator : Attribute
    {
        public abstract bool Validate(object value);
    }
}