﻿using System;

namespace LapraDB.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ModelAttribute : Attribute
    {
        public ModelAttribute(string tableName)
        {
            TableName = tableName;
        }

        public string TableName { get; }
    }

}