using System;
using System.Collections.Generic;
using System.Linq;

namespace LapraDB.Backend
{
    public abstract class Backend
    {
        public virtual string OrderQuerySet(string str)
        {
            // TODO syntax checking here!
            if (string.IsNullOrEmpty(str))
                return "";

            var identifiers = str.Split(',');

            var sqlScript = string.Format(" ORDER BY {0}",
                string.Join(", ", identifiers.Select(i => i.StartsWith("-") ? $"`{i.Substring(1)}` DESC" : $"`{i}` ASC")));

            return sqlScript;
        }

        public virtual KeyValuePair<string, Dictionary<string, object>> GetQuerySet(
            string table, IEnumerable<string> fields, Q query, int page, int limit, string order = "",
            Dictionary<string, object> values = null)
        {
            var fieldsString = fields == null ?
                "*" : string.Join(", ", fields.Select(f => $"`{f}`"));

            var sqlString = $"SELECT {fieldsString} FROM `{table}`";

            if (query != null)
            {
                var querySet = query.WhereQuerySet(values);
                
                if (querySet != null)
                {
                    values ??= querySet.Item2;

                    sqlString += " WHERE " + querySet.Item1;
                }
            }

            if (order != "")
            {
                var orderString = OrderQuerySet(order);
                sqlString += orderString;
            }

            if (limit > 0)
                sqlString += $" LIMIT {page * limit}, {limit}";

            return new KeyValuePair<string, Dictionary<string, object>>(sqlString, values);
        }


        public virtual KeyValuePair<string, Dictionary<string, object>> CountQuerySet(
            string table, Q query)
        {
            var sqlString = $"SELECT COUNT(*) FROM `{table}`";

            Dictionary<string, object> values = null;

            if (query != null)
            {
                var querySet = query.WhereQuerySet();

                if (querySet != null)
                {
                    values = querySet.Item2;

                    sqlString += " WHERE " + querySet.Item1;
                }
            }

            return new KeyValuePair<string, Dictionary<string, object>>(sqlString, values);
        }

        public virtual KeyValuePair<string, Dictionary<string, object>> CreateQuerySet(
            string table, Dictionary<string, object> values)
        {
            var sqlString = string.Format(
                "INSERT INTO `{0}` ({1}) VALUES ({2})",
                table,
                string.Join(", ", values.Select(value => $"`{value.Key}`")),
                string.Join(", ", values.Select(value => $"@{value.Key}"))
            );
            return new KeyValuePair<string, Dictionary<string, object>>(sqlString, values);
        }


        public virtual KeyValuePair<string, Dictionary<string, object>> UpdateQuerySet(
            string table, Dictionary<string, object> values, Q query)
        {
            var queryData = query.WhereQuerySet();

            var sqlString = string.Format(
                "UPDATE `{0}` SET {1} WHERE {2}",
                table,
                string.Join(", ", values.Select(value => $"`{value.Key}` = @{value.Key}")),
                queryData.Item1
            );

            values = values.Union(queryData.Item2).ToDictionary(v => v.Key, v => v.Value);

            return new KeyValuePair<string, Dictionary<string, object>>(sqlString, values);
        }

        public virtual KeyValuePair<string, Dictionary<string, object>> DeleteQuerySet(
            string table, Q query)
        {
            // most always be provided unless you want to like... destroy everything
            var data = query.WhereQuerySet();

            var sqlString = $"DELETE FROM `{table}` WHERE {data.Item1}";

            return new KeyValuePair<string, Dictionary<string, object>>(
                    sqlString, data.Item2);
        }

        public abstract string ColumnInfoQuerySet(string name);

        public abstract string GetLastIdQuerySet();

        public static string GetTableName(Type model) => Model.TableName[model];

        public virtual List<object> Get(Type model, object pkValue)
        {
            var tableName = GetTableName(model);
            var pkField = Model.PkField[model];

            var fields = Model.GetReadFields(model).Select(e => e.Key);

            return Lapra.SqlExecute(GetQuerySet(tableName, fields, new Q(pkField.Key, pkValue), 0, 1))
                .FirstOrDefault();
        }

        public virtual List<List<object>> List(Type model, Q query = null, int page = 0, int limit = 0, string order = "")
        {
            if (page < 0) throw new ArgumentOutOfRangeException(nameof(page));
            if (limit < 0) throw new ArgumentOutOfRangeException(nameof(limit));

            var tableName = GetTableName(model);

            var fields = Model.GetReadFields(model).Select(e => e.Key);

            return Lapra.SqlExecute(GetQuerySet(tableName, fields, query, page, limit, order));
        }

        public virtual long Count(Type model, Q query = null)
        {
            var tableName = GetTableName(model);

            return Convert.ToInt64(Lapra.SqlExecute(CountQuerySet(tableName, query)).FirstOrDefault().FirstOrDefault());
        }

        public virtual bool Create(Model model)
        {
            var modelType = model.GetType();

            var data = model.GetWriteFields().ToDictionary(e => e.Key, e => e.Value.GetValue(model));

            // generate with the sql data
            Lapra.SqlExecute(CreateQuerySet(GetTableName(modelType), data));

            model.LoadFromList(Get(modelType, GetLastId()));

            model.IsNew = false;

            return true;
        }

        public virtual bool Update(Model model)
        {
            var modelType = model.GetType();

            // we cannot update anything...
            if (!model.HasFieldUpdated()) return true;

            // I need to get all the available models to get stuff
            var data = model.GetUpdatedFields().ToDictionary(e => e.Key, e => e.Value.GetValue(model));

            Lapra.SqlExecute(UpdateQuerySet(GetTableName(modelType), data,
                new Q(model.PkFieldName, model.PkFieldValue)));

            model.LoadFromList(Get(modelType, model.PkFieldValue));

            return true;
        }

        public virtual bool Delete(Model model)
        {
            var modelType = model.GetType();

            Lapra.SqlExecute(DeleteQuerySet(GetTableName(modelType), new Q(model.PkFieldName, model.PkFieldValue)));

            return true;
        }

        public virtual object GetLastId()
        {
            var sqlString = GetLastIdQuerySet();

            return Lapra.SqlExecute(sqlString, null)[0][0];
        }

        public virtual List<List<object>> ColumnInfo()
        {
            var sqlString = ColumnInfoQuerySet(Lapra.ModelValidationDatabaseName);
            return Lapra.SqlExecute(sqlString, null);
        }
    }
}
