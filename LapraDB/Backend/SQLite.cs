using System.Collections.Generic;

namespace LapraDB.Backend
{
    public class SQLiteBackend : Backend
    {
        public override string ColumnInfoQuerySet(string name)
        {
            return $"SELECT '{name}' AS `table_name`, cid, name, type, pk FROM PRAGMA_TABLE_INFO('{name}')";
        }

        public override string GetLastIdQuerySet()
        {
            return "SELECT last_insert_rowid()";
        }

        public override List<List<object>> ColumnInfo()
        {
            // Since we don't have the abilyt to query all the table columns in SQLite
            // we need to do it manually... deafeating the purpose of doing it all at once.

            // we don't need it
            var tables = Lapra.SqlExecute(
                    "SELECT name FROM sqlite_schema WHERE type = 'table' AND name NOT LIKE 'sqlite_%'",
                    null);

            var data = new List<List<object>>();

            foreach(var table in tables)
            {
                var sqlString = ColumnInfoQuerySet((string)table[0]);
                data.AddRange(Lapra.SqlExecute(sqlString, null));
            }

            return data;
        }
    }
}
