using System.Collections.Generic;

namespace LapraDB.Backend
{
    public class MySQL : Backend
    {
        public override string ColumnInfoQuerySet(string name)
        {
            return "SELECT TABLE_NAME, ORDINAL_POSITION, COLUMN_NAME, DATA_TYPE, IS_NULLABLE, COLUMN_KEY"
                + $" FROM information_schema.columns WHERE table_schema = '{Lapra.ModelValidationDatabaseName}'"
                + " ORDER BY TABLE_NAME, ORDINAL_POSITION";
        }

        public override string GetLastIdQuerySet()
        {
            return "SELECT LAST_INSERT_ID()";
        }
    }
}
