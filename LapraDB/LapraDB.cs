using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using LapraDB.Attributes;
using NLog;
using NLog.Targets;

namespace LapraDB
{
    public static class Lapra
    {
        public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// SqlExecute is the handler for all Database related things that `LapraDB` can use.
        /// </summary>
        public static Func<string, Dictionary<string, object>, List<List<object>>> SqlExecuteFunc;

        public static Backend.Backend Backend;
        public static bool ModelValidation = true;

        public static string ModelValidationDatabaseName { get; set; } = null;
        public static bool ModelValidationIgnoreIfBigger { get; set; } = true;
        public static bool ModelValidationIgnoreNames { get; set; } = true;
        public static bool ModelValidationIgnoreTypes { get; set; } = true;

        /// <summary>
        /// It provides a way to throw an error or continue executing code without crashing if not
        /// in debug
        /// </summary>
        /// <param name="sqlScript"> The actual script to be executed in the database </param>
        /// <param name="arguments"> The arguments for the SqlExecute handler </param>
        /// <returns> Returns the result of the reader in the format of list of list of objects </returns>
        public static List<List<object>> SqlExecute(string sqlScript, Dictionary<string, object> arguments)
        {
            return SqlExecuteFunc != null ? SqlExecuteFunc(sqlScript, arguments) : new List<List<object>>();
        }

        public static List<List<object>> SqlExecute(KeyValuePair<string, Dictionary<string, object>> data)
        {
            return SqlExecute(data.Key, data.Value);
        }

        private static void ModelInitializer(Type model)
        {
            Dictionary<string, PropertyInfo> fields = new();
            Dictionary<string, PropertyInfo> writeFields = new();
            Dictionary<string, PropertyInfo> readFields = new();

            Model.Fields.Add(model, fields);
            Model.WriteFields.Add(model, writeFields);
            Model.ReadFields.Add(model, readFields);

            var tableAttribute =
                (ModelAttribute)model.GetCustomAttributes().First(attr => attr.GetType() == typeof(ModelAttribute));
            Model.TableName.Add(model, tableAttribute.TableName);

            foreach (var field in model.GetProperties())
            {
                var fieldAttr = (FieldAttribute)field.GetCustomAttribute(typeof(FieldAttribute), true);
                if (fieldAttr == null) continue;

                fields.Add(fieldAttr.Name, field);

                if (fieldAttr.readOnly && fieldAttr.writeOnly)
                {
                    Logger.Warn("{0}.{1}({2}) Have both readOnly and writeOnly as true, ignoring attributes",
                        model.Name, field.Name, fieldAttr.Name);
                }
                else
                {
                    /* if not read only it means that it can be written */
                    if (!fieldAttr.readOnly) writeFields.Add(fieldAttr.Name, field);
                    /* if not write only it means it can be read*/
                    if (!fieldAttr.writeOnly) readFields.Add(fieldAttr.Name, field);
                }

                // if we found it try to search if it is also a Pk
                if (!Model.PkField.ContainsKey(model) && field.GetCustomAttributes().Any(
                        attr => attr.GetType() == typeof(PrimaryKeyAttribute)))
                {
                    Model.PkField.Add(model, new KeyValuePair<string, PropertyInfo>(fieldAttr.Name, field));
                }
            }

            if (!Model.PkField.ContainsKey(model))
                Logger.Warn($"No PK was found for class '{model}'");
        }


        private static readonly Dictionary<string, Type> Types = new()
        {
            // we cannot really differentiate from it in the type that it
            // is return.
            {"int",       typeof(int)},
            {"tinyint",   typeof(byte)},
            {"enum",      typeof(string)},
            {"varchar",   typeof(string)},
            {"double",    typeof(double)},
            {"decimal",   typeof(decimal)},
            {"timestamp", typeof(DateTime)},
            {"datetime",  typeof(DateTime)},
            {"blob",      typeof(byte[])},
        };

        private static void AddToError(Dictionary<string, List<string>> errors, string key, string error)
        {
            if(!errors.ContainsKey(key))
                errors.Add(key, new List<string>());

            errors[key].Add(error);
        }

        /// <summary>
        /// With the current number of initialized models. we validate them in the database.
        /// We ignore if some extra tables are found in the database. but we raise an error if
        /// a Initialized model it's not in the database.
        ///
        /// It also checks that the name of the model fields are the same as the database.
        /// we ignore if the database's table has more columns.
        ///
        /// This checks if the position of each columns corresponds to the position of each column in the other side
        /// </summary>
        public static Dictionary<string, List<string>> ModelValidate()
        {
            var errors  = new Dictionary<string, List<string>>();

            var dbInfo = Backend.ColumnInfo().GroupBy(
                    e => (string)e[0]).ToDictionary(e => e.Key, e => e.ToList());

            var modelsInfo = Model.TableName
                .Select(e => (e.Value, Model.Fields[e.Key]
                            .ToDictionary(i => i.Key, i=> i.Value.PropertyType)))
                .ToDictionary(e => e.Item1, e => e.Item2);

            foreach (var modelInfo in modelsInfo)
            {
                if (!dbInfo.ContainsKey(modelInfo.Key))
                {
                    // add an error about the one.
                    AddToError(errors, modelInfo.Key, "Model not found in the current database");
                    continue;
                }

                var dbColumns = dbInfo[modelInfo.Key];

                /// the real check begins now!

                // Check if our model it's bigger than the database's
                var diff = dbColumns.Count.CompareTo(modelInfo.Value.Count);
                if (diff == -1 && !ModelValidationIgnoreIfBigger)
                {
                    AddToError(errors, modelInfo.Key,
                            "Model's number has more columns than the " +
                            $"database's table! ({modelInfo.Value.Count}>{dbColumns.Count})");
                }

                // Check column names
                if (ModelValidationIgnoreNames)
                {
                    var mismatchesName = dbColumns
                        .Zip(modelInfo.Value, (x, y) => ((string)x[2], y.Key))
                        .Where(e => e.Item1 != e.Item2).ToList();

                    foreach (var mismatch in mismatchesName)
                        AddToError(errors, modelInfo.Key,
                                $"name mismatch: [model].`{mismatch.Item2}` != [table].`{mismatch.Item1}`");
                }

                // Check column types
                if (ModelValidationIgnoreTypes)
                {
                    var mismatchesType = dbColumns
                        .Zip(modelInfo.Value, (x, y) => (y.Key, x[3], y.Value))
                        .Where(e => (Type)e.Item2 != e.Item3).ToList();

                    foreach (var mismatch in mismatchesType)
                        AddToError(errors, modelInfo.Key,
                                "type mismatch: " + 
                                $"[model].{mismatch.Item1}.{mismatch.Item3} != " + 
                                $"[table].{mismatch.Item1}.{mismatch.Item2}");
                }
            }

            return errors;
        }

        public static void LapraInitialize()
        {
            // config current logger
            var config = new NLog.Config.LoggingConfiguration();

            var logFile = new ConsoleTarget("LapraDB");

            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);

            LogManager.Configuration = config;

            try
            {
                var models = from a in AppDomain.CurrentDomain.GetAssemblies()
                    from t in a.GetTypes()
                    where t.IsDefined(typeof(ModelAttribute), true)
                    select t;

                foreach (var model in models)
                    ModelInitializer(model);

                var errors = new Dictionary<string, List<string>>();

                if (ModelValidation)
                    errors = ModelValidate();

                if (errors.Count > 0)
                {
                    var errorString = string.Join(
                            "\n", errors.Select(e => $"{e.Key}: " + string.Join("\n\t", e.Value)));

                    Logger.Warn(errorString);
                }
            }
            catch (ReflectionTypeLoadException)
            {
            }
        }

        public static void LapraInitialize(
            Backend.Backend backend,
            Func<string, Dictionary<string, object>, List<List<object>>> function)
        {
            Backend = backend;
            SqlExecuteFunc = function;

            LapraInitialize();
        }

        /// <summary>
        /// Limit search by one. obtaining the first element
        /// </summary>
        /// <param name="pk">the pk can be any object really. but necessary for the query</param>
        /// <typeparam name="T">Model</typeparam>
        /// <returns>Returns an instance of the object</returns>
        public static T Get<T>(object pk) where T : Model
        {
            // checks if the given Primary Key it's a null value
            if (pk == null)
                return null;

            // try to get an item and cast it
            var type = typeof(T);

            var values = Backend.Get(type, pk);

            if (values == null)
                return null;

            // if it did not fail then proceed.
            var instance = Activator.CreateInstance<T>();
            instance.LoadFromList(values);

            return instance;
        }

        public static T Get<T>(Q query, string order = "") where T : Model
        {
            var type = typeof(T);

            var values = Backend.List(type, query, limit: 1, order: order).FirstOrDefault();

            if (values == null)
                return null;

            var instance = Activator.CreateInstance<T>();
            instance.LoadFromList(values);

            return instance;
        }

        public static QuerySet
            GetQuery<T>(IEnumerable<string> fields, Q query = null,
                int page = 0, int limit = 0, string order = "") where T : Model
        {
            var type = typeof(T);

            return new QuerySet(type, fields, query, page, limit, order);
        }

        public static List<T> GetList<T>(Q query = null, int page = 0, int limit = 0, string order = "")
            where T : Model
        {
            var iterable = new List<T>();

            var values = Backend.List(typeof(T), query, page, limit, order);

            foreach (var value in values)
            {
                var instance = Activator.CreateInstance<T>();
                instance.LoadFromList(value);
                iterable.Add(instance);
            }

            return iterable;
        }

        public static long Count<T>(Q query) where T : Model
        {
            return Backend.Count(typeof(T), query);
        }
    }
}
