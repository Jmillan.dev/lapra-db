using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using LapraDB.Attributes;

namespace LapraDB
{
    public abstract class Model : ICloneable, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private static uint _uid;

        public static readonly Dictionary<Type, Dictionary<string, PropertyInfo>> Fields = new();
        public static readonly Dictionary<Type, Dictionary<string, PropertyInfo>> WriteFields = new();
        public static readonly Dictionary<Type, Dictionary<string, PropertyInfo>> ReadFields = new();
        public static readonly Dictionary<Type, KeyValuePair<string, PropertyInfo>> PkField = new();
        public static readonly Dictionary<Type, string> TableName = new();

        /* STATIC METHODS */
        public static Dictionary<string, PropertyInfo> GetFields(Type model) => Fields[model];
        public static Dictionary<string, PropertyInfo> GetWriteFields(Type model) => WriteFields[model];
        public static Dictionary<string, PropertyInfo> GetReadFields(Type model) => ReadFields[model];

        public static PropertyInfo GetField(Type model, string name) => GetFields(model)[name];
        public static List<string> GetFieldKeys(Type model) => GetFields(model).Select(field => field.Key).ToList();

        public static string GetPkFieldName(Type type) => PkField[type].Key;
        public static string GetPkFieldName<T>() where T : Model => PkField[typeof(T)].Key;

        /* LOCAL METHODS*/
        public Dictionary<string, PropertyInfo> GetFields() => Fields[GetType()];
        public Dictionary<string, PropertyInfo> GetWriteFields() => WriteFields[GetType()];
        public Dictionary<string, PropertyInfo> GetReadFields() => ReadFields[GetType()];
        public Dictionary<string, PropertyInfo> GetUpdatedFields() => WriteFields[GetType()]
            .Where(e => _tableValues.Contains(e.Key)).ToDictionary(e => e.Key, e => e.Value);

        public PropertyInfo GetField(string name) => GetFields()[name];
        public object GetFieldValue(string name) => GetFields()[name].GetValue(this);
        public void SetFieldValue(string name, object value) => GetFields()[name].SetValue(this, value);
        public List<object> GetListOfValues() => GetFields().Select(field => field.Value.GetValue(this)).ToList();
        public List<string> GetFieldKeys() => GetFields().Select(field => field.Key).ToList();

        public string PkFieldName => PkField[GetType()].Key;
        public object PkFieldValue => PkField[GetType()].Value.GetValue(this);

        private void OnPropertyChanged(string propertyName) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private readonly HashSet<string> _tableValues = new();
        private bool _loadingValues = false;

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;

            field = value;

            if (propertyName != null && !_loadingValues)
            {
                var fieldName = ((FieldAttribute)GetType().GetProperty(propertyName)?
                    .GetCustomAttribute(typeof(FieldAttribute)))?.Name;

                if (fieldName != null)
                {
                    _tableValues.Add(fieldName);
                }
            }

            OnPropertyChanged(propertyName);

            return true;
        }

        public bool FieldUpdated(string name) =>
            !IsNew && _tableValues.Contains(name);

        public bool HasFieldUpdated() =>
            !IsNew && _tableValues.Count > 0;

        /// <summary>
        /// Flag mentioning if the model is brand new
        /// </summary>
        public bool IsNew = true;

        protected Model()
        {
            Uid = _uid++;
        }

        /// <summary>
        /// This is the unique number used for identifying objects in lists
        /// </summary>
        public uint Uid { get; }

        /// <summary>
        /// Duplicates this instance.
        /// Note that this instance has all the exact values as the original one
        /// </summary>
        public object Clone()
        {
            var instance = (Model)Activator.CreateInstance(GetType());

            instance.CopyFieldsFrom(this);
            instance.IsNew = IsNew;

            return instance;
        }

        protected virtual bool SaveBefore() => true;

        protected virtual void SaveAfter(bool created)
        {
        }

        /// <summary>
        /// Save the current model into the database.
        /// This function can create a new entry or update one
        /// </summary>
        /// <returns>A boolean indicating if it saved or not</returns>
        public bool Save()
        {
            if (!SaveBefore()) return false;

            var isCreating = PkFieldValue == null | IsNew;

            var result = isCreating ? Lapra.Backend.Create(this) : Lapra.Backend.Update(this);

            if (!result) return false;

            SaveAfter(isCreating);

            return true;
        }

        /// <summary>
        /// Delete this Object from the Database if it has a pk Value
        /// </summary>
        public void Delete()
        {
            if (IsNew) return;
            Lapra.Backend.Delete(this);
            IsNew = true;
        }

        /// <summary>
        /// With a given list of values it can Initialize the model entry.
        /// If a list of fields was given, then It will try to read the values in the
        /// order of the list. If no list was given then it will try to read all the
        /// values of the model in order.
        /// * It will not try to read values that are flag as `IsReadOnly`
        /// </summary>
        /// <param name="values"> The values to read from </param>
        /// <param name="maskFields"> Optional: If given it will try to read the list of values in this order </param>
        public void LoadFromList(List<object> values, List<string> maskFields = null)
        {
            IsNew = false;
            _tableValues.Clear();
            _loadingValues = true;

            var index = 0;

            // We should never get the values in WriteOnly
            var readFields = GetReadFields();

            // We can mask the ReadFields with the maskFields
            if (maskFields != null)
                readFields = readFields.Where(field => maskFields.Contains(field.Key))
                    .ToDictionary(field => field.Key, field => field.Value);

            foreach (var field in readFields)
            {
                var value = values[index++];

                field.Value.SetValue(this, value is DBNull ? null : value);
            }

            _loadingValues = false;
        }

        /// <summary>
        /// Reload the data of this entry from the database
        /// </summary>
        public void ReloadFromDb()
        {
            if (IsNew) return;

            LoadFromList(Lapra.Backend.Get(GetType(), PkFieldValue));
        }

        /// <summary>
        /// TODO
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public T Related<T>() where T : Model
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Copy from another instance of the same model it's fields.
        /// It ignores by Default the PK value
        /// </summary>
        /// <param name="other">The object to copy from</param>
        /// <exception cref="InvalidCastException">The Model must be the same type as the one calling</exception>
        public virtual void CopyFieldsFrom(Model other)
        {
            if (GetType() != other.GetType())
                throw new ArgumentException(
                    string.Format("expected {0} but got {1}", GetType().Name, other.GetType().Name));

            foreach (var field in GetFields())
                field.Value.SetValue(this, field.Value.GetValue(other));
        }
    }
}
