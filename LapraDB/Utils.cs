﻿using System.Collections.Generic;

namespace LapraDB
{
    public static class Utils
    {
        public const string FormatDate = "yyyy-MM-dd";
        public const string FormatDatetime = "yyyy-MM-dd HH:mm:ss";

        /// <summary>
        /// Really inefficient code... and ugly.
        /// I just don't know how to do it better with C#
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static List<KeyValuePair<string, string>> CompareLists(List<string> a, List<string> b)
        {
            // decide who is the bigger and who is the smallest
            var mismatch = new List<KeyValuePair<string, string>>();
            var lists = a.Count < b.Count
                ? new KeyValuePair<List<string>, List<string>>(a, b)
                : new KeyValuePair<List<string>, List<string>>(b, a);

            for (var i = 0; i < lists.Key.Count; i++)
            {
                if (lists.Key[i] != lists.Value[i])
                    mismatch.Add(new KeyValuePair<string, string>(lists.Key[i], lists.Value[i]));
            }

            return mismatch;
        }
    }
}