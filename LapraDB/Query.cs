using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel.Design;
using System.Linq;
using NLog.LogReceiverService;

namespace LapraDB
{
    public class QueryException : Exception
    {
        public QueryException(string message) : base(message)
        {
        }
    }

    // similar to the node in drf-query-filter
    public class QData
    {
        private const string LikeScape = "/";
        public static void HandleString(QData self, object that)
        {
            // gets the object's string representation
            var value = (string)self.Value;
            var format = (string)that;

            // we need to sanitize the string
            value = value.Replace(LikeScape, LikeScape+"/");
            value = value.Replace("%", LikeScape+"%");
            value = value.Replace("_", LikeScape+"_");

            self.ExpressionFormat = "{0} {1} {2} ESCAPE '"+ LikeScape +"'";

            if (!string.IsNullOrEmpty(format))
                value = string.Format(format, value);

            self.Value = value;
        }

        private static void HandleIn(QData self, object that)
        {
            switch (self.Value)
            {
                case QuerySet:
                    // nothing to be done
                    return;
                default:
                    if (typeof(IEnumerable).IsAssignableFrom(self.Value.GetType()))
                    {
                        self._valueIterable = true;
                        break;
                    }

                    throw new InvalidCastException("object it's not a 'IEnumerable<object>' or " +
                                                   "'LapraDB.Query.QuerySet'");
            }
        }

        private static readonly Dictionary<string, Tuple<string, string, string, string, object, Action<QData, object>>>
            ExpressionTable = new () {
                // conversion
                /*Expression,        NOT Format, SQL Exp,  Field Format,  Param Format, Extra, Handler*/
                {"date",        new (     null,    null,  "DATE({0})",          null,    null, null)},
                {"date>",       new (     null,    null,         null,   "DATE({0})",    null, null)},
                {"date<>",      new (     null,    null,  "DATE({0})",   "DATE({0})",    null, null)},
                {"year",        new (     null,    null,  "YEAR({0})",          null,    null, null)},
                {"month",       new (     null,    null, "MONTH({0})",          null,    null, null)},
                {"lower",       new (     null,    null, "LOWER({0})",          null,    null, null)},
                {"upper",       new (     null,    null, "UPPER({0})",          null,    null, null)},
                {"md5>",        new (     null,    null,         null,    "MD5({0})",    null, null)},

                // string related
                {"like",        new ("NOT {0}",  "LIKE",         null,         null,  "{0}" , HandleString)},
                {"contains",    new ("NOT {0}",  "LIKE",         null,         null, "%{0}%", HandleString)},
                {"icontains",   new ("NOT {0}",  "LIKE", "LOWER({0})", "LOWER({0})", "%{0}%", HandleString)},
                {"startswith",  new ("NOT {0}",  "LIKE",         null,         null,  "{0}%", HandleString)},
                {"istartswith", new ("NOT {0}",  "LIKE", "LOWER({0})", "LOWER({0})",  "{0}%", HandleString)},
                {"endswith",    new ("NOT {0}",  "LIKE",         null,         null, "%{0}" , HandleString)},
                {"iendswith",   new ("NOT {0}",  "LIKE", "LOWER({0})", "LOWER({0})", "%{0}" , HandleString)},

                // others
                {"in",          new ("NOT {0}",    "IN",         null,         null,    null, HandleIn)},
                // ignores the calling value
                {"isnull",      new (     null,    "IS",         null,       "NULL",    null, null)},
                {"isnotnull",   new (     null,"IS NOT",         null,       "NULL",    null, null)},

                // Arithmetic operations
                {"e",           new (   "!{0}",     "=",         null,         null,    null, null)},
                {"gt",          new (   "!{0}",     ">",         null,         null,    null, null)},
                {"lt",          new (   "!{0}",     "<",         null,         null,    null, null)},
                {"gte",         new (     null,    ">=",         null,         null,    null, null)},
                {"lte",         new (     null,    "<=",         null,         null,    null, null)},
            };
        // we need to scape stuff if said operation it's given!
        //
        private readonly string _name;
        private readonly object _originalValue;

        private string _nameFormat = "`{0}`";
        private string _paramFormat = "@{0}";

        public string ExpressionFormat = "{0} {1} {2}";
        private string _expressionToken = "=";
        private bool _expressionHandled;

        private bool _valueIterable = false;

        public string OriginalName => _name;
        public string Name => string.Format(_nameFormat, _name);
        public string ParamName(string name) => string.Format(_paramFormat, name);
        public object Value { get; set; }


        public QData(string name, object value)
        {
            var nameList = name.Split('$');
            _name = nameList.First();
            Value = _originalValue = value;

            // check the name for new expressions!
            foreach (var expression in nameList.Skip(1))
                Expression(expression);
        }

        private void HandleNameFormat(string format) => _nameFormat = string.Format(format, _nameFormat);
        private void HandleParamFormat(string format) => _paramFormat = string.Format(format, _paramFormat);

        private void HandleExpression(string negate, string expression)
        {
            if (_expressionHandled) throw new QueryException("Expression already handle before!");

            _expressionToken = string.IsNullOrEmpty(negate) ? expression : string.Format(negate, expression);
            _expressionHandled = true;
        }

        private void Expression(string expression)
        {
            expression = expression.ToLower();

            var flagNot = expression.StartsWith("not");

            if (flagNot) expression = expression.Substring(3);

            if (ExpressionTable.ContainsKey(expression))
            {
                var data = ExpressionTable[expression];

                if (!string.IsNullOrEmpty(data.Item2))
                {
                    if (flagNot && string.IsNullOrEmpty(data.Item1))
                        throw new QueryException("Expression does not support `not`");
                    HandleExpression(flagNot ? data.Item1 : null, data.Item2);
                }

                if (!string.IsNullOrEmpty(data.Item3))
                    HandleNameFormat(data.Item3);

                if (!string.IsNullOrEmpty(data.Item4))
                    HandleParamFormat(data.Item4);

                data.Item6?.Invoke(this, data.Item5);
            }
            else
            {
                // TODO try to tell him which one it's the closest one.
                throw new ArgumentOutOfRangeException($"{expression} it's not a valid expression");
            }
        }

        private string InsertIntoValues(
                IDictionary<string, object> values, string name)
        {
            var count = values.Count(v => v.Key == name);
            if (count > 0)
                name = $"{name}{count + 1}";

            if (_valueIterable)
            {
                var index = 0;
                var names = new List<string>();
                foreach (var value in (IEnumerable)Value)
                {
                    var nameItem = $"{name}_{index++}";
                    names.Add(ParamName(nameItem));
                    values.Add(nameItem, value);
                }

                return $"({string.Join(", ", names)})";
            }

            values.Add(name, Value);
            return ParamName(name);
        }

        public string GetExpression(Dictionary<string, object> values)
        {
            // if we have an expression already calculated we should probably
            // fix that script...
            //
            // If the value it's a QuerySet the rules are changed a bit...
            // instead of adding it as a ParamName we add it as a statement...
            // since it will probably will have some stuff inside
            string expressionString;

            if (Value is QuerySet)
            {
                expressionString = string.Format(
                        ExpressionFormat,
                        Name, _expressionToken,
                        string.Format(
                            "({0})",
                            ((QuerySet)Value).GetStatement(values)));
            }
            else
            {
                expressionString = string.Format(
                    ExpressionFormat,
                    Name, _expressionToken,
                    InsertIntoValues(values, "w_" + OriginalName));
            }

            return expressionString;
        }
    }

    public class Q
    {
        public enum Type
        {
            And,
            Or
        }

        private readonly List<Q> _children;
        public readonly QData _data;
        private readonly Type _type;

        public Q(Type type)
        {
            _children = new List<Q>();
            _data = null;
            _type = type;
        }

        public Q(string name, Type type = Type.And) : this(name, null, type)
        {
        }

        public Q(string name, object value, Type type = Type.And)
        {
            _data = new QData(name, value);
            _type = type;
            _children = new List<Q>();
        }

        public Q(IReadOnlyCollection<Tuple<string, object>> values, Type type = Type.And)
        {
            var first = values.First();
            if (first != null)
                _data = new QData(first.Item1, first.Item2);

            _children = values.Skip(1).Select(value => new Q(value.Item1, value.Item1)).ToList();
        }

        public static Q operator &(Q left, Q right)
        {
            if (left._type == Type.And)
            {
                left._children.Add(right);
                return left;
            }

            var container = new Q(Type.And);
            container._children.Add(left);
            container._children.Add(right);

            return container;
        }

        public static Q operator |(Q left, Q right)
        {
            if (left._type == Type.Or)
            {
                left._children.Add(right);
                return left;
            }

            var container = new Q(Type.Or);
            container._children.Add(left);
            container._children.Add(right);

            return container;
        }

        public override string ToString()
        {
            // we can return a basic interpretation of the query... not exactly the final queryset...
            // but more for debugging purposes.
            var str = "";

            if (_data != null)
                str += _data.Name;

            if (_children.Count <= 0) return str;

            var type = _type == Type.And ? " & " : " | ";

            if (_data != null)
                str += type;
            str += string.Join(type,
                _children.Select(c => c._children.Count > 0 ? $"({c})" : c.ToString()));

            return str.Trim(); // just to make sure no loose ends.
        }

        private string _WhereQuerySet(Dictionary<string, object> values)
        {
            var builder = "";

            if (_data != null)
                builder += _data.GetExpression(values);

            // TODO add support to render the representation of the QuerySet.

            if (_children.Count == 0) return builder;

            var type = _type == Type.And ? " AND " : " OR ";

            if (_data != null)
                builder += type;

            builder += string.Join(type,
                _children.Select(
                    c => c._children.Count > 0 ? $"({c._WhereQuerySet(values)})" : c._WhereQuerySet(values)));

            return builder;
        }

        public Tuple<string, Dictionary<string, object>>
            WhereQuerySet(Dictionary<string, object> values = null)
        {
            if (_data == null && _children.Count == 0) return null;

            values ??= new Dictionary<string, object>();

            return new Tuple<string, Dictionary<string, object>>(_WhereQuerySet(values), values);
        }
    }

    public class QuerySet
    {
        // has built in functions
        private readonly Type _type;
        private readonly string _tableName;
        private readonly IEnumerable<string> _fields;
        private readonly Q _query;
        private readonly int _page;
        private readonly int _limit;
        private readonly string _order;

        public QuerySet(Type type, IEnumerable<string> fields, Q query,
                int page, int limit, string order)
        {
            // I don't know if this can be done???
            _type = type;
            _tableName = Model.TableName[_type];
            _fields = fields;
            _query = query;
            _page = page;
            _limit = limit;
            _order = order;
        }

        public QuerySet(string tableName, IEnumerable<string> fields, Q query,
                int page, int limit, string order)
        {
            // I don't know if this can be done???
            _tableName = tableName;
            _fields = fields;
            _query = query;
            _page = page;
            _limit = limit;
            _order = order;
        }

        public string GetStatement(Dictionary<string, object> values)
        {
            // It should generate a string statement with the given as:
            // Of course this it's defined by the Backend
            // SELECT {fields} FROM {table} \
            //  [WHERE {expressions,...}] [ORDER BY {expressions, ...}] \
            //  [LIMIT {start}, {end}]
            return Lapra.Backend.GetQuerySet(
                    _tableName, _fields, _query, _page, _limit, _order,
                    values).Key;
        }
    }
}
