using System;
using System.Collections.Generic;
using System.Data.SQLite;
using LapraDB;
using LapraDB.Backend;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LapraDBTestUnit
{
    [TestClass]
    public class GetQuerySet
    {
        [TestMethod]
        public void TestGetQuerySet()
        {
            var backend = new MySQL();
            var data = backend.GetQuerySet("users", null, null, 0, 0, "-date_created");

            Assert.AreEqual("SELECT * FROM `users` ORDER BY `date_created` DESC", data.Key);
        }
    }
}

