using LapraDB;
using LapraDB.Attributes;
using LapraDB.Attributes.Validators;


namespace LapraDBTestUnit
{
    [Model("user")]
    public class User : Model
    {
        // Uhg!
        private long _id;
        private string _name;
        private string _email;

        [Field("id", "Unique number"), PrimaryKey]       public long Id { get => _id; set => SetField(ref _id, value); }
        [Field("name", "User's name"), MaxLength(255)]   public string Name { get => _name; set => SetField(ref _name, value); }
        [Field("email", "User's email"), MaxLength(255)] public string Email { get => _email; set => SetField(ref _email, value); }
    }
}
