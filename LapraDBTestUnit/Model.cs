using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using LapraDB;
using LapraDB.Backend;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LapraDBTestUnit
{
    [TestClass]
    public class ModelTest
    {
        /// <summary>
        /// Simple Sql Executer. It executes an sql script with arguments in a safe way....
        /// It is just needed for injecting into my awful librarie of Objects
        /// </summary>
        /// <param name="sqlString">SQL Script</param>
        /// <param name="arguments">Arguments</param>
        /// <returns>Return a list of lists of objects</returns>
        private static readonly SQLiteConnection Conn = new("Data Source=:memory:");

        public static List<List<object>> SqlExecute(string sqlString, Dictionary<string, object> arguments)
        {
            var cmd = new SQLiteCommand(sqlString, Conn);
            if (arguments != null)
                foreach (var (key, value) in arguments)
                    cmd.Parameters.AddWithValue("@" + key, value);

            // Generate the list here
            var result = new List<List<object>>();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var row = new List<object>();
                for (var i = 0; i < reader.FieldCount; i++)
                    row.Add(reader.GetValue(i));
                result.Add(row);
            }

            return result;
        }

        [TestMethod]
        public void TestModel()
        {
            Conn.Open();
            SqlExecute("CREATE TABLE user(id INTEGER PRIMARY KEY, name VARCHAR(255) NOT NULL, email VARCHAR(255))",
                null);

            Lapra.ModelValidation = false;
            // Lapra.ModelValidationIgnoreTypes = false;
            Lapra.LapraInitialize(new SQLiteBackend(), SqlExecute);

            // var errors = Lapra.ModelValidate();
            // if (errors.Count > 0)
            // {
            //     var errorString = string.Join(
            //             "\n", errors.Select(e => $"{e.Key}: " + string.Join("\n\t", e.Value)));
            //     Console.WriteLine(errorString);
            // }
            // else
            //     Console.WriteLine("No errors!");

            var user = new User
            {
                Name = "Miguel",
                Email = "sardinas@domain.xcv"
            };

            user.Save();

            Console.WriteLine($"User's new id: {user.Id}");

            user.Name = "Something new";

            user.Save();

            var userB = Lapra.Get<User>(user.Id);

            Assert.AreEqual(user.Name, userB.Name);

            userB.Name = "UserB change this!";

            userB.Save();

            user.ReloadFromDb();

            Assert.AreEqual(user.Name, userB.Name);

            user.Delete();

            user = Lapra.Get<User>(user.Id);
            // should be null

            Assert.AreEqual(user, null);

            Conn.Close();
        }
    }
}
