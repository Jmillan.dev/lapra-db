using LapraDB;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Collections;
using System;
using LapraDB.Backend;

namespace LapraDBTestUnit
{
    [TestClass]
    public class QueryTest
    {
        [TestMethod]
        public void TestQuery()
        {
            var query = new Q("name$contains", "value")
                        & new Q("birthday", "2020-12-31");
            Assert.AreEqual(
                "`name` LIKE @w_name ESCAPE '/' AND `birthday` = @w_birthday",
                query.WhereQuerySet().Item1);

            query = new Q("name$iContains", "value")
                    & new Q("birthday", "2020-12-31")
                    | (new Q("id", "0")
                       & new Q("name", "junk_user"));

            Assert.AreEqual(
                "(LOWER(`name`) LIKE LOWER(@w_name) ESCAPE '/' AND `birthday` = @w_birthday) OR (`id` = @w_id AND `name` = @w_name2)",
                query.WhereQuerySet().Item1);

            query = (new Q("is_admin", "0")
                     | new Q("is_superuser", "0"))
                    & new Q("birthday$gte", "2020-12-12");

            Assert.AreEqual(
                "(`is_admin` = @w_is_admin OR `is_superuser` = @w_is_superuser) AND `birthday` >= @w_birthday",
                query.WhereQuerySet().Item1);
        }

        /// <summary>
        /// Validate if the in can obtain list values that are not exactly
        /// `IEnumerable<\?>`
        /// </summary>
        [TestMethod]
        public void TestIn()
        {
            // just in case
            Lapra.Backend = new MySQL();
            var querySet = new QuerySet("users", new []{"id"}, null, 0, 0, "-date_created");

            var queryA = new Q("value$in", querySet);

            Assert.AreEqual(
                "`value` IN (SELECT `id` FROM `users` ORDER BY `date_created` DESC)",
                queryA.WhereQuerySet().Item1);

            var queryB = new Q("value$in", new []{30, 40, 50});

            Assert.AreEqual(
                "`value` IN (@w_value_0, @w_value_1, @w_value_2)",
                queryB.WhereQuerySet().Item1);

            var queryC = new Q("value$in", new List<int>{10, 20, 30});

            Assert.AreEqual(
                "`value` IN (@w_value_0, @w_value_1, @w_value_2)",
                queryC.WhereQuerySet().Item1);
        }
    }
}
